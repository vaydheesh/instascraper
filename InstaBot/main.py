from playwright.sync_api import sync_playwright

from InstaBot.Extractor.extractor import ExtractInsta
from InstaBot.response_handler import ResponseHandler
from InstaBot.utils import show_more_posts


def main():
    with sync_playwright() as p:
        profile_handle = "sadhguru"
        url = f'https://www.instagram.com/{profile_handle}/'
        browser = p.chromium.launch(
            executable_path="/usr/bin/chromium",
            headless=False,
        )
        page = browser.new_page()
        response_capture = ResponseHandler(scroll_count=5)
        page.on("response", lambda response: response_capture.capture_response(response))
        page.goto(url)
        show_more_posts(page, profile_handle, response_capture)
        print(response_capture.response_body)
        e = ExtractInsta(page.content())
        user_details = e.extract_user_details()
        posts = list(e.extract_post_details())
        igtv = list(e.extract_igtv_details())
        print(user_details)
        print(posts)
        print(igtv)

        browser.close()


if __name__ == '__main__':
    main()
