import time

from playwright.sync_api import Page

from InstaBot.response_handler import ResponseHandler


def show_more_posts(page: Page, username, response_capture: ResponseHandler):
    print('show more posts')
    time.sleep(10)
    page.click(f"text=Show More Posts from {username}")
    time.sleep(10)
    response_capture.scroll_count -= 1
